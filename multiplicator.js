var net = require('net');
var clients = [];
var server_ip = "127.0.0.1";
var server_port = 6000;
var client_port = 5000;

var client = new net.Socket();
client.connect(server_port, server_ip, function() {

});

client.on('data', function(data) {
    broadcast(data);
});

client.on('close', function() {
	console.log('Connection closed');
}); 

// Start a TCP Server
var server = net.createServer(function (socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 

  // Put this new client in the list
  clients.push(socket);

  // Handle incoming messages from clients.
  socket.on('data', function (data) {

  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
  });   

}).listen(client_port);

// Send a message to all clients
function broadcast(message) {
    clients.forEach(function (client) {
        client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
}
